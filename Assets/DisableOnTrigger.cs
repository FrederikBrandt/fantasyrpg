﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnTrigger : MonoBehaviour {
    [SerializeField]
    GameObject objectToBeActivated;
    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Player"))
        {
            if(objectToBeActivated != null) objectToBeActivated.SetActive(true);
            gameObject.SetActive(false);
        } 
    }
}
