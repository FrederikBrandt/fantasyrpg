﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class PlayerStats : MonoBehaviour {
    public string playerName = "player";
    public float health = 20;
    public float strength = 20; //strength to calculate damage - with multiplier from weapon
    public float vitality = 10;
    public int weaponID = 0; //0 is fists
    public Element weaponElement;
    public string attackAnimation;
    [SerializeField]
    ParticleSystem particleSystem;

    [SerializeField]
    ParticleSystem overcharge;

    float attackCooldown;
    float attackCooldownTimer;
    float holdingTimer;

    float attackRange;
    float damageMultiplier;

    LoadWeapons weapons;
    Animator anim;

    RaycastHit hit;

    public Vector3 resetPos;

    public void TakeDamage(float damage, Element damageElement, Vector3 damageFromWhere)
    {
        if (health - damage < 0) Death();
        else health -= damage;
        particleSystem.transform.LookAt(damageFromWhere);
        particleSystem.Play();
    }

    void Death()
    {
        Debug.Log("You have been killed");
        health = 10f;
        transform.position = resetPos;
    }

    private void Start()
    {
        resetPos = transform.position;
        GameObject.Find("ManagerPlayers").GetComponent<PlayerManager>().playerStats.Add(this);
        weapons = GameObject.Find("ManagerExternalData").GetComponent<LoadWeapons>();
        anim = GetComponentInChildren<Animator>();
        UpdateWeapon(0);
    }

    private void Update()
    {
        if(attackCooldownTimer < Time.time)
        {
            if (Input.GetMouseButton(0))
            {
                holdingTimer += Time.deltaTime;
                string test = weapons.animations[weapons.iDnumber.IndexOf(weaponID)];
                test = test.Substring(0, test.Length - 1);
                anim.Play(test, -1, 0f);
            }
            if (Input.GetMouseButtonUp(0))
            {
                if (holdingTimer > 3f)
                {
                    ChargedAttack();
                }
                else Attack();
                holdingTimer = 0f;
            }
        }
        
    }


    public void UpdateWeapon(int weaponIDToBeUpdated)
    {
        weaponID = weaponIDToBeUpdated;
        attackCooldown = weapons.attackSpeed[weapons.iDnumber.IndexOf(weaponID)];
        attackRange = weapons.attackRange[weapons.iDnumber.IndexOf(weaponID)];
        //if(weaponIDToBeUpdated == 0)//Solve strange char in EmptyHit
        //{
            attackAnimation = weapons.animations[weapons.iDnumber.IndexOf(weaponID)];
            attackAnimation = attackAnimation.Substring(0, attackAnimation.Length - 1);
        //}
        //else attackAnimation = weapons.animations[weapons.iDnumber.IndexOf(weaponID)];
        weaponElement = weapons.element[weapons.iDnumber.IndexOf(weaponID)];
        damageMultiplier = weapons.multiplier[weapons.iDnumber.IndexOf(weaponID)];
        GetComponent<ModelWeapons>().SwapWeapon(weapons.iDName[weapons.iDnumber.IndexOf(weaponID)]);
    }


    void Attack()
    {

        string test = weapons.animations[weapons.iDnumber.IndexOf(weaponID)];
        test = test.Substring(0, test.Length - 1);
        //anim.Play(test, 0, 0f);
        anim.Play(test, 1,0);
        Ray ray = new Ray(transform.position + Vector3.up, transform.forward);
        if(Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag("Enemy") && hit.distance < attackRange)
            {
                hit.transform.GetComponent<EnemyStats>().TakeDamage(CalculateDamage(0.6f), weapons.element[weapons.iDnumber.IndexOf(weaponID)]);
                Debug.Log("Attacked enemy has " + hit.transform.GetComponent<EnemyStats>().health);
                attackCooldownTimer = Time.time + attackCooldown;
                if (weaponID == 0) GetComponent<AudioSource>().Play();
            }
        }

    }

    void ChargedAttack()
    {

        string test = weapons.animations[weapons.iDnumber.IndexOf(weaponID)];
        test = test.Substring(0, test.Length - 1);
        anim.Play(test, 1, 0f);
        Ray ray = new Ray(transform.position + Vector3.up, transform.forward);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.CompareTag("Enemy") && hit.distance < attackRange)
            {
                hit.transform.GetComponent<EnemyStats>().TakeDamage(CalculateDamage(0.7f), weapons.element[weapons.iDnumber.IndexOf(weaponID)]);
                Debug.Log("Attacked enemy has " + hit.transform.GetComponent<EnemyStats>().health);
                attackCooldownTimer = Time.time + attackCooldown;
                overcharge.Play();
            }
        }
    }

    float CalculateDamage(float basicMultiplier)
    {
        return  basicMultiplier * strength * damageMultiplier;
    }
}
