﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Animator anim;

    public Transform nearestEnemy;
    public float movementSpeed;

    Vector3 direction = new Vector3();
    Rigidbody rb;

    public bool pressingE;
    float resetE = 1f;
    float resetTimer;
    Transform interactible;
    bool disableRota;

    // Update is called once per frame
    void FixedUpdate()
    {
        disableRota = false;
        if (Input.GetKey(KeyCode.A)) direction = -transform.right;
        if (Input.GetKey(KeyCode.D)) direction += transform.right;
        if (Input.GetKey(KeyCode.S))
        {
            direction += -transform.forward;
            disableRota = true;
        }
        if (Input.GetKey(KeyCode.W)) direction += transform.forward;


        if (Input.GetKey(KeyCode.A)) direction = -Camera.main.transform.right;
        if (Input.GetKey(KeyCode.D)) direction += Camera.main.transform.right;
        if (Input.GetKey(KeyCode.S))
        {
            direction += -Camera.main.transform.forward;
            disableRota = true;
        }
        if (Input.GetKey(KeyCode.W)) direction += Camera.main.transform.forward;

        direction.y = 0;

        if (Input.GetKeyDown(KeyCode.E))
        {
            pressingE = true;
            resetTimer = Time.time + resetE;
        }
        if (pressingE && resetTimer < Time.time) pressingE = false;

        if (direction != Vector3.zero)
        {
            anim.Play("Run");
            if (Input.GetKey(KeyCode.LeftShift))
            {
                rb.velocity = (direction.normalized) * (movementSpeed * 1.5f);
            }
            else rb.velocity = (direction.normalized) * movementSpeed;
            direction = Vector3.zero;
        }
        
        if (nearestEnemy != null && Input.GetKey(KeyCode.Space)) transform.LookAt(new Vector3(nearestEnemy.position.x,transform.position.y,nearestEnemy.position.z));
        else if(rb.velocity != Vector3.zero && !disableRota)transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rb.velocity, Vector3.up), Time.deltaTime * 16f);
        if (rb.velocity.magnitude < 0.7f)
        {
            rb.velocity = Vector3.zero;
            if(anim.GetCurrentAnimatorStateInfo(0).IsName("Run"))anim.Play("Idle");
        }

    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Interactible")) coll.GetComponent<Interactible>().playerInteration = GetComponent<PlayerStats>();
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.CompareTag("Interactible")) coll.GetComponent<Interactible>().playerInteration = null;
    }
}
