﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour {

    public float damage;

    NavMeshAgent agent;
    float searchTimer;
    public float searchInterval;
    public float visibilityArea;
    PlayerManager playerManager;

    public float attackCooldown;
    float attackTimer;
    Animator anim;

    float roamTimer;
    public float roamTimerCooldown;

    Vector3 startPos;

	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        playerManager = GameObject.Find("ManagerPlayers").GetComponent<PlayerManager>() ;
        anim = GetComponent<Animator>();
        agent.SetDestination(transform.position);
        startPos = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (searchTimer < Time.time)
        {
            searchTimer = Time.time + searchInterval;
            StartCoroutine("SearchForPlayer");
        }

        if(agent.velocity.magnitude < 0.5f && roamTimer < Time.time)
        {
            Roam();
        }
	}

    IEnumerator SearchForPlayer()
    {
        foreach(PlayerStats ps in playerManager.playerStats)
        {
            float distance = Vector3.Distance(transform.position, ps.transform.position);
            if (distance < visibilityArea)
            {
                if (distance < 1.5f)
                {
                    agent.SetDestination(transform.position);
                    if (attackTimer < Time.time)
                    {
                        AttackPlayer(ps);
                        attackTimer = Time.time + attackCooldown;
                    }
                }
                else
                {
                    agent.SetDestination(ps.transform.position);
                    anim.Play("Run");
                }
                searchTimer = Time.time;
                if (ps.GetComponent<PlayerMovement>().nearestEnemy == null || Vector3.Distance(ps.GetComponent<PlayerMovement>().nearestEnemy.position, ps.transform.position) > Vector3.Distance(transform.position, ps.transform.position)) ps.GetComponent<PlayerMovement>().nearestEnemy = transform;
            }
        }
        return null;
    }

    void Roam()
    {
        roamTimer = roamTimerCooldown + Time.time + Random.Range(1f, 5f);
        agent.SetDestination(startPos + new Vector3(Random.Range(0.5f,1.5f), 0f, Random.Range(0.5f, 1.5f)));
    }

    void AttackPlayer(PlayerStats ps)
    {
        anim.Play("HitEmpty", -1, 0f);
        ps.TakeDamage(damage,Element.Physical,transform.position);

    }
}
