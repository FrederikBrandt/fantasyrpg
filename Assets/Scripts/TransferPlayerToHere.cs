﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransferPlayerToHere : MonoBehaviour {

	// Use this for initialization
	void Start () {
        foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player")){
            go.transform.position = transform.position;
            go.GetComponent<PlayerStats>().resetPos = transform.position;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
