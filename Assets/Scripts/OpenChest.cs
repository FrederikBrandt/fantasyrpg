﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenChest : MonoBehaviour {
    Interactible inter;
    bool used;
    bool opening;
    public int weapon;
    public Transform lid;
    private float time;
    public float opentime;
    float offset;
    bool loop = true;
    Vector3 stuckPos;

    [SerializeField]
    GameObject containedItem;

    [SerializeField]
    Light light;

    PlayerStats playerWhoOpenedMe;

	// Use this for initialization
	void Start () {
        inter = GetComponent<Interactible>();
        offset = transform.eulerAngles.y;
	}
	
	// Update is called once per frame
	void Update () {
		if(inter.playerInteration != null && !used || opening){
            if (loop && inter.playerInteration.GetComponent<PlayerMovement>().pressingE)
            {
                loop = false;
                stuckPos = inter.playerInteration.transform.position;
                light.intensity = 6;
            } 
            if (loop) return;
            if (!opening) playerWhoOpenedMe = inter.playerInteration;
            opening = true;
            OpenAnim();

        }

        if (opening)
        {
            time += Time.deltaTime;
            playerWhoOpenedMe.transform.position = stuckPos;
            playerWhoOpenedMe.transform.LookAt(transform);

        }
    }

    void OpenAnim()
    {

        lid.transform.eulerAngles=(Vector3.Lerp(transform.eulerAngles,new Vector3(-118,offset,0),time/opentime));

        if (time > opentime)
        {
            opening = false;
            //playerWhoOpenedMe.UpdateWeapon(weapon);
            playerWhoOpenedMe.GetComponent<Inventory>().AddWeaponToInventory(weapon, playerWhoOpenedMe);
            used = true;
            Destroy(light);
            Destroy(containedItem);
        }

    }
}
