﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelWeapons : MonoBehaviour {
    public List<Transform> weapons;

    private void Start()
    {
        foreach(Transform t in weapons)
        {
            if(t.name != "Fists")t.gameObject.SetActive(false);
        }
    }

    public void SwapWeapon(string weaponName)
    {
        foreach (Transform t in weapons)
        {
            if (t.name == weaponName) t.gameObject.SetActive(true);
            else t.gameObject.SetActive(false);
        }
    }
}
