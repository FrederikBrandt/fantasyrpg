﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour {
    public string enemyName = "enemy";
    public float health = 3;
    public float strength = 10; //strength to calculate damage - with multiplier from weapon
    public float vitality = 10;
    public int weaponID = 0; //0 is fists
    [SerializeField]
    ParticleSystem particleSystem;

    public void TakeDamage(float damage, Element damageElement)
    {
        if (health - damage < 0) Death();
        else health -= damage;
        Debug.Log(damage + " damage taken, " + health + " health remaining");
        particleSystem.Play();

    }

    void Death()
    {
        Destroy(gameObject);
    }
}
