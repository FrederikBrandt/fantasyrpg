﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public List<PlayerStats> playerStats = new List<PlayerStats>();
    public List<PlayerMovement> playerMovement = new List<PlayerMovement>();
}
