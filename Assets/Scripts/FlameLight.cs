﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameLight : MonoBehaviour {

    float g = .35f;
    float gb = .35f;
    float time;
    float timeNow;
    bool upd;
    float intEnd = 1;
    float intStart = 1;

    Light light;
    Color color = new Color(1f,0f,0f);

	// Use this for initialization
	void Start () {

        light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {

        if (!upd)
        {
            upd = true;   
            UpdateCol();
        }

        color.g = (g - gb) * (timeNow / time) + gb;
       // light.color = color;
        light.intensity = (intEnd - intStart) * (timeNow / time) + intStart; 
        
        timeNow += Time.deltaTime;

        if (timeNow > time)
        {
            upd = false;
        }
    }

    void UpdateCol()
    {
        time = Random.Range(.3f,.7f);
        gb = g;
        g = Random.Range(.45f, .50f);
        intEnd = intStart;
        intStart = Random.Range(.75f, 1f);
        timeNow = 0;
    }

}
