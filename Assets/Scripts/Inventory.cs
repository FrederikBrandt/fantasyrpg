﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {
    List<int> weaponIDsOnHand = new List<int>();

    LoadWeapons weapons;

    [SerializeField]
    GameObject buttonPrefab;
    [SerializeField]
    GameObject scrollView;

    private void Start()
    {
        weapons = GameObject.Find("ManagerExternalData").GetComponent<LoadWeapons>();
    }

    public void AddWeaponToInventory(int weaponID,PlayerStats ps)
    {
        GameObject go = GameObject.Instantiate(buttonPrefab);
        go.GetComponentInChildren<Text>().text = weapons.iDName[weapons.iDnumber.IndexOf(weaponID)];
        go.transform.SetParent(scrollView.transform);
        go.GetComponent<Weapon>().playerStats = ps;
        go.GetComponent<Weapon>().inventory = this;
    }
}
