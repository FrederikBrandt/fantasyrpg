﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ResourcesLoadCSV : MonoBehaviour {
    public TextAsset textAsset;

    public List<string> iDnumber = new List<string>();
    public List<string> iDName = new List<string>();

    // Use this for initialization
    void Awake()
    {
        SplitToWords("CSV", "AbilityList");
    }

    public void SplitToWords(string folder, string file)
    {
        textAsset = Resources.Load(Path.Combine(folder, file)) as TextAsset;
        string[] longString = textAsset.text.Split('\n');// Hver linje er nu splittet ud til et punkt i et array.
        for (int i = 1; i < longString.Length;i++) //Start med 1 da 0 er "overskriften"
        {
            string[] words = longString[i].Split(';');
            iDnumber.Add(words[0]);//Feks ability navn
            iDName.Add(words[1]);//Feks ability damage multiplier
        }
        textAsset = null;//fjern fra hukommelse
    }
}
