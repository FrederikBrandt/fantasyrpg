﻿using System.IO;
using System;
using System.Collections.Generic;
using UnityEngine;

public class LoadWeapons : MonoBehaviour {

    public TextAsset textAsset;

    public List<int> iDnumber = new List<int>();
    public List<string> iDName = new List<string>();
    public List<Element> element = new List<Element>();
    public List<float> multiplier = new List<float>();
    public List<float> attackSpeed = new List<float>();
    public List<float> attackRange = new List<float>();
    public List<string> animations = new List<string>();


    // Use this for initialization
    void Awake()
    {
        SplitToWords("CSV", "Weapons");
    }

    public void SplitToWords(string folder, string file)
    {
        textAsset = Resources.Load(Path.Combine(folder, file)) as TextAsset;
        string[] longString = textAsset.text.Split('\n');// Hver linje er nu splittet ud til et punkt i et array.
        for (int i = 1; i < longString.Length; i++) //Start med 1 da 0 er "overskriften"
        {
            string[] words = longString[i].Split(';');
            iDnumber.Add(int.Parse(words[0]));//Feks ability navn
            iDName.Add(words[1]);//Feks ability damage multiplier
            element.Add((Element)Enum.Parse(typeof(Element), words[2])); //Tilføj element enum
            multiplier.Add(float.Parse(words[3]));
            attackSpeed.Add(float.Parse(words[4]));
            attackRange.Add(float.Parse(words[5]));
            animations.Add(words[6]);
        }
        textAsset = null;//fjern fra hukommelse
    }
}

public enum Element
{
    Physical
}
