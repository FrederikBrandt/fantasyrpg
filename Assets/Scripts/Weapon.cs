﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : MonoBehaviour {

    public PlayerStats playerStats;
    public Inventory inventory;
    int currentWeapon;

    LoadWeapons weapons;

    public void ClickMe()
    {
        currentWeapon = playerStats.weaponID;
        Text text = GetComponentInChildren<Text>();
        playerStats.UpdateWeapon(weapons.iDName.IndexOf(text.text));
        text.text = weapons.iDName[weapons.iDnumber.IndexOf(currentWeapon)];

    }
    private void Start()
    {
        weapons = GameObject.Find("ManagerExternalData").GetComponent<LoadWeapons>();
        if (playerStats.weaponID == 0) ClickMe();
    }
}
