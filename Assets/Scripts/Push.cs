﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Push : MonoBehaviour {
    Interactible inter;
    [SerializeField]
    Vector3[] newPos;
    public float timer;
    Vector3[] oldPos;
    float internalTimer;

    [SerializeField]
    Transform[] objectsToBePushed;

    public bool newMap;
    public int mapIndexToLoad;

    bool run;
	// Use this for initialization
	void Start () {
        inter = GetComponent<Interactible>();
        oldPos = new Vector3[objectsToBePushed.Length];
        for (int i = 0; i < objectsToBePushed.Length; i++)
        {
            oldPos[i] = objectsToBePushed[i].position;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (inter.playerInteration != null) run = true;
        if (!run) return;

        internalTimer += Time.deltaTime;
        if (internalTimer > timer)
        {
            internalTimer = timer;
            run = false;
            
            if (newMap)
            {
                SceneManager.LoadSceneAsync(mapIndexToLoad);
            } 
            

        }
        for (int i = 0; i < objectsToBePushed.Length; i++)
        {
            objectsToBePushed[i].position = Vector3.Lerp(oldPos[i], newPos[i], internalTimer / timer);
        }

	}
}
