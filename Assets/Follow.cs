﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public GameObject target;
    public float damping = 1;
    Vector3 offset;
    Vector3 tempPos;
    void Start()
    {
        offset = target.transform.position - transform.position;
    }

    void LateUpdate()
    {
        float currentAngle = transform.eulerAngles.y;
        float desiredAngle = target.transform.eulerAngles.y;
        float angle = Mathf.LerpAngle(currentAngle, desiredAngle, damping);

        Quaternion rotation = Quaternion.Euler(0, angle, 0);
        tempPos = target.transform.position - (rotation * offset);
        tempPos.y = 2f;
        transform.position = tempPos;

        transform.LookAt(target.transform);
    }
}
